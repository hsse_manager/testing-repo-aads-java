#! /usr/bin/env bash

test -s test_file
if [[ $1 == "master" ]]
then 
  echo "No testing on master branch"
elif [[ $1 == "main" ]]
then
    echo "No testing on main branch"
else 
  echo "begin testing $1"
  cp testing_repo/format.jar $1/format.jar
  cd $1
  if [[ ! $? -eq 0 ]]
  then 
    echo "А где папка с именем ветки?"
    exit 1
  fi

  java Main.java < ../testing_repo/$1.txt
  if [[ ! $? -eq 0 ]]
  then 
    echo "Решение не работает:("
    exit 1
  fi
  echo "Вы потрясающие!"
fi
